/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 10, 2013, 4:33 PM
 */

#include <iostream>

using namespace std;


/*
 * 
 */
int main(int argc, char** argv) {

int singles, doubles, triples, Home_runs, at_bats;
double Slugging_percentage;

cout << "Let's see how good a slugger you really are....\n";
cout << "Enter them singles!\n";
cin >> singles;
cout << "Now the doubles!\n";
cin >> doubles;
cout << "And the triples!\n";
cin >> triples;
cout << "Lastly, homers!\n";
cin >> Home_runs;
cout << "I lied! At bats, please!\n";
cin >> at_bats;
Slugging_percentage = (singles + 2.0 * doubles + 3.0 * triples + 
        4.0 * Home_runs) / at_bats;
cout << "Your slugging percentage is " << Slugging_percentage << "%!";

    return 0;
}

