/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 26, 2013, 4:59 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
  int n;
    double guess, fguess, r, check, lguess;
    
    cout << "We're gonna square some roots!\n";
    cout << "Enter a numer, son:  ";
    cin >> n;
    cout << "\n";
    
    guess = n / 2;
    
    do
    {
          r = n / guess;
          lguess = guess;
          guess = (guess + r) / 2.0;
          check = lguess - guess;
          
    }while ( check > lguess * .01);
    
   
    cout << "The square root of " << n << " is " << guess << endl;
    return 0;
}

