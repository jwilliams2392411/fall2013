/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 5, 2013, 1:25 PM
 */

#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "*************************************************\n";
    cout << "\n";        
    cout << "           CCC             SSSS      !!\n";
    cout << "         C     C          S     S    !!\n";
    cout << "        C                S           !!\n";
    cout << "       C                  S          !!\n";
    cout << "       C                   SSSS      !!\n";
    cout << "       C                        S    !!\n";
    cout << "        C                        S   !!\n";
    cout << "         C     C          S     S      \n";
    cout << "           CCC              SSSS     00\n";
    cout << "\n";
    cout << "\n";
    cout << "*************************************************\n";
    cout << "    Computer Science is Cool Stuff!!!\n";        
    return 0;
}

