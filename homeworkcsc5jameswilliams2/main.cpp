/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 5, 2013, 1:11 PM
 */

#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
 int first_number, second_number, sum, product;
    
    cout << "Hello\n";
    cout << "Press return after entering a number. \n";
    cout << "Enter the first number:\n";
    
    cin >> first_number;
    
    cout << "Enter the second number:\n";
    cin >> second_number;
    sum = first_number + second_number;
    product = first_number * second_number;
    cout << "If you take ";
    cout << first_number;
    cout << " and ";
    cout << second_number;
    cout << " and do a little magic,\n";
    cout << "you get ";
    cout << sum;
    cout << " and ";
    cout << product;
    cout << ", yo.\n";
    cout << "This is the end of the program.\n";
    return 0;
}

