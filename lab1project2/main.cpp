/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 3, 2013, 4:57 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
int a = 5;
int b = 10;
cout << "a: " << a << " " << "b: " << b << endl;
a = b;
b = 5;
cout << "a: " << a << " " << "b: " << b << endl;

    return 0;
}

