/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 3, 2013, 3:05 PM
 */

#include <cstdlib>
#include <iostream> //Allows use of CIN and COUT

using namespace std; //location of libraries

/*
 * 
 */
int main(int argc, char** argv) {

    int num = 3;
    int age = 10;
    //cout << "How old are you" << endl;
    cout << "How old are you" << endl;
    
    cin >> num;
    cout << "Enter another age\n";
    cin >> age;
    
    cout << "You Are " << age << " or " << num
            << " years old";
    return 0;
}

