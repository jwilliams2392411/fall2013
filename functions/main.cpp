/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 26, 2013, 3:27 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    char grade, sign;
    cout << "Prepare for judgment!\n";
    cout << "Tell me thine grade, and thou shalt know thine GPA!" << endl;
    cin.get(grade);
    cin.get(sign);
    
    switch (grade)
    {
        case 'A':
            switch (sign)
            {
                case '+':
                    cout <<"This is a 4.0!\n";
                    break;
                case '-':
                    cout << "This is a 3.7!\n";
                    break;
                default:
                    cout << "This is a 4.0!\n";
            }
            break;
        case 'B':
            switch (sign)
            {
                case '+':
                    cout << "This is a 3.3!\n";
                    break;
                case '-':
                    cout << "This is a 2.7!\n";
                    break;
                default:
                    cout << "This is a 3.0!\n";
            }
            break;
        default:
            cout << "Try harder next time! No output for you!\n";
         
      
    }
    return EXIT_SUCCESS;
}

