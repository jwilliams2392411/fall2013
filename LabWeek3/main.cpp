/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 10, 2013, 4:13 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    double purchase, payment; 
    int change, changeD, changeQ, changeDi, changeN, changeP;
    cout << "What're ya buyin'?\n";
    cout << "Input Purchase Amount: ";
    cin >> purchase;
    cout << "How much you payin'?\n";
    cin >> payment;
    change = payment - purchase;
    change * 100;
    changeD = change / 1.00;
    change = change % changeD;
    changeQ = change / .25;
    change = change % changeQ;
    changeDi = change / .10;
    change = change % changeDi;
    changeN = change / .05;
    change = change % changeN;
    changeP = change / .01;
    cout << "Your change is " << changeD << " dollars, " << changeQ << 
            " quarters, " << changeDi << " dimes, " << changeN << " nickles, "
            << "and " << changeP << " pennies!";
    


    return 0;
}

